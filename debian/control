Source: r-cran-sys
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-sys
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-sys.git
Homepage: https://cran.r-project.org/package=sys
Standards-Version: 4.6.1
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev
Testsuite: autopkgtest-pkg-r

Package: r-cran-sys
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: Powerful and Reliable Tools for Running System Commands in GNU R
 Drop-in replacements for the base system2() function with fine control
 and consistent behavior across platforms. Supports clean interruption,
 timeout, background tasks, and streaming STDIN / STDOUT / STDERR over
 binary or text connections. Arguments on Windows automatically get
 encoded and quoted to work on different locales. On Unix platforms the
 package also provides functions for evaluating expressions inside a
 temporary fork. Such evaluations have no side effects on the main R
 process, and support reliable interrupts and timeouts. This provides the
 basis for a sandboxing mechanism.
